
import { Component,OnInit } from '@angular/core';
import { JsonPipe } from '@angular/common';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.css'
})

export class HeaderComponent  implements OnInit {
  loginStatus:any;
  adminStatus:any;
  flag:boolean;
  cartCount: number = 0;
  // cartItem:any;
 
  constructor(private  service: UserService, router: Router) {
    this.flag=false;
    this.service.cartCount$.subscribe(count => {
      this.cartCount = count;
      });
  }
async  ngOnInit() {
    
    await  this.service.getUserLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
    await this.service.getAdminLoginStatus().subscribe((data: any) => {
      this.adminStatus = data;
    });
    if( this.adminStatus==true || this.loginStatus==true){
      this.flag=true;
      console.log(this.flag)

  }else{
    console.log(this.adminStatus +""+this.loginStatus);
  }
  }

   
  
}
