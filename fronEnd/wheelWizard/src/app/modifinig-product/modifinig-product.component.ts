import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-modifinig-product',
  templateUrl: './modifinig-product.component.html',
  styleUrl: './modifinig-product.component.css'
})
export class ModifinigProductComponent {
product:any;
id:any;
constructor(private service: UserService,private route: ActivatedRoute,private router: Router){
    
}
async ngOnInit(){
  await this.route.queryParams.subscribe(params => {
    this.id = params['id'];
   
  });
 this.product= await this.service.getProdutById(this.id);
}
async updateProduct(item:any){
    this.product.name= item.name,
    this.product.imgsrc= item.imgsrc,
    this.product.categoryID= item.categoryID,
    this.product.cost= item.cost,
    this.product.desciption= item.desciption
  console.log(this.product);
await  this.service.updateProduct(this.product);
this.router.navigate(['editProducts']);
}

}

