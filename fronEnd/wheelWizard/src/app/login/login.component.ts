import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  user: any;
  constructor(private service: UserService, private router: Router,private toastr: ToastrService) {
  }
  
 async loginSubmit(loginForm: any) {
    console.log(loginForm);
    //Setting EmailId under LocalStorage
    localStorage.setItem('emailId', loginForm.emailId);
   
    if (loginForm.emailId == "admin" && loginForm.password == "Admin@123") {
      // this.toastr.success('Hello world!', 'Toastr fun!');
      this.service.setAdminLoggedIn();
      this.router.navigate(['admin']);
    } else {  
      await this.service.userLogin(loginForm.emailId, loginForm.password).then((data: any) => {
        console.log(data);
        localStorage.setItem('user',data);
        this.user = data;
      });
    }
      console.log(this.user)
      if (this.user != null) {
        localStorage.setItem('user', JSON.stringify(this.user));
        this.service.setUserLoggedIn();
        this.toastr.success('welcome ','' +this.user.emailId);
        this.router.navigate(['/']);
      } else {
        this.toastr.error('Invalid Credentials','error');
      }
    }
  }