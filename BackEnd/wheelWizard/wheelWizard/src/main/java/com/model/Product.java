package com.model;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {
	@Id@GeneratedValue
	private int Pid; 
    private String name;
    private String imgsrc; 
    private int categoryID;
    public Product(){
    	
    }
	public Product(int pid, String name, String imgsrc, int categoryID) {
		super();
		Pid = pid;
		this.name = name;
		this.imgsrc = imgsrc;
		this.categoryID = categoryID;
	}
	public int getPid() {
		return Pid;
	}
	public void setPid(int pid) {
		Pid = pid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImgsrc() {
		return imgsrc;
	}
	public void setImgsrc(String imgsrc) {
		this.imgsrc = imgsrc;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	@Override
	public String toString() {
		return "ProductC [Pid=" + Pid + ", name=" + name + ", imgsrc=" + imgsrc + ", categoryID=" + categoryID + "]";
	}
}