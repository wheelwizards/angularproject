package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.TempCartDao;

import com.model.TempCart;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class TempCartController {
	@Autowired
	TempCartDao tempDao;
	
	@PostMapping("SetCart")
	public void setTempCart(@RequestBody List<TempCart> cart) {
		tempDao.setTempCart(cart);
	}
	@GetMapping("getCart")
	public List<TempCart> getTempCartByCId( ){
		return tempDao.getTempCart();	
	}
	@PutMapping("addIterms")
	public void addIterms(@RequestBody TempCart cart) {
		 tempDao.addIterms(cart);
	}
	@GetMapping("getCart/{id}")
	public List<TempCart> getTempCartByCId(@PathVariable("id") int id ){
		return tempDao.getTempCartByCId(id);	
	}
	@PutMapping("decrementTempCart")
	public void decrementTempCart(@RequestBody TempCart cart){
		 tempDao.decrementTempCart(cart);
	} 
	
	@DeleteMapping("deleteById/{id}")
	public void  deletecartItermsById(@PathVariable("id") int id) {
		 tempDao.deletecartItermsById(id);
		
	}


}
