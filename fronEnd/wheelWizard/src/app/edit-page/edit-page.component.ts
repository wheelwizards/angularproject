import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrl: './edit-page.component.css'
})
export class EditPageComponent implements OnInit {
id:any;
user:any;
currentUser:any;
countries:any;
  constructor(private service: UserService,private route: ActivatedRoute,private router: Router){
    
  }
  async ngOnInit(){
    await this.route.queryParams.subscribe(params => {
      this.id = params['id'];
     
    });
  
    this.service.getAllContries().subscribe((data: any) => { this.countries = data; });
    this.currentUser= await this.service.getUserInfoById(parseInt(this.id));
  }
 async editForm(form:any){
  this.user = await  {
      id:parseInt(this.id),
      userName: form.userName,
      address: form.address,
      gender: form.gender,
      country: form.country,
      mobileNo: form.mobileNo,
      emailId: form.emailId,
      password: this.currentUser.password
    }
   await this.service.updateUserInfo(this.user);
    this.router.navigate(['admin']);
    console.log(this.user)
  }
}
