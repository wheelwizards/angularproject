import { Component,OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import * as AOS from 'aos';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit {
  id: number;
  products:any;
  temp:any;
  cartProducts: any;
  cartItem:any;
  user:any;
  logInStatus:any;
  constructor(private route: ActivatedRoute,private service :UserService,private router :Router,private toastr: ToastrService) {
    this.id=0;
    this.cartItem={};
    this.temp=localStorage.getItem('user');
    this.user=JSON.parse(this.temp);
    this.cartProducts = [];
    this.service.getUserLoginStatus().subscribe((data: any) => {
      this.logInStatus = data;
    });
  }

 async ngOnInit() {
  AOS.init( {
    duration:2300
  });
   await this.route.queryParams.subscribe(params => {
      this.id = params['id'];
     
    });
     await this.service.getProduct(this.id).then((data: any) => {
    this.products=data;
    });
  
   console.log(this.products);
  }
 async addToCart(product: any) {

 
    if(!this.logInStatus){
      this.toastr.error('Please login','error');
      this.router.navigate(['login']);
    }
    else{
    this.cartProducts.push(product);
   await this.service.setTempCart(this.cartItem);
   this.cartItem=[];
    this.service.updateCartCount(this.calculateCartCount());
    this.toastr.success('added to cart','success');
    
    // localStorage.setItem('cartProducts', JSON.stringify(this.cartProducts));

    }
    this.cartItem=({
      cartId: product.id,
      cost: product.cost,
      img: product.imgsrc,
      productName: product.name,
      quantity: 1, 
      user: {
        id: this.user.id},
    });
    console.log( JSON.stringify(this.cartItem))
    console.log(!this.logInStatus)
  }
private calculateCartCount(): number {
  // Calculate the count of items in the cart
  return this.cartProducts.length;
  }
}
