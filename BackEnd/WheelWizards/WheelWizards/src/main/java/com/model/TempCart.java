package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class TempCart {
	@Id@GeneratedValue
	private int sNo;
	
	private int cartId;
	private String img;
	private String productName;
	private int cost;
	private int quantity;
	
	@ManyToOne
	@JoinColumn(name="Id")
	UserInfo user;
	public TempCart(){
		
	}
	public TempCart(int sNo, int cartId, String img, String productName, int cost, int quantity, UserInfo user) {
		super();
		this.sNo = sNo;
		this.cartId = cartId;
		this.img = img;
		this.productName = productName;
		this.cost = cost;
		this.quantity = quantity;
		this.user = user;
	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public int getCartId() {
		return cartId;
	}

	public void setCartId(int cartId) {
		this.cartId = cartId;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public UserInfo getUser() {
		return user;
	}

	public void setUser(UserInfo user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "TempCart [sNo=" + sNo + ", cartId=" + cartId + ", img=" + img + ", productName=" + productName
				+ ", cost=" + cost + ", quantity=" + quantity + ", user=" + user + "]";
	}
	
}
