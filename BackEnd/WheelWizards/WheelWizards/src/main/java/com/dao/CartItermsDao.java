package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.CartItrems;

@Service
public class CartItermsDao {
	@Autowired
	CartItermsRepository cartRepo;

	public void  setCartItrems(List<CartItrems> cartIterms) {
		// TODO Auto-generated method stub
		for(CartItrems cartIterm:cartIterms){
			cartRepo.save(cartIterm);
		}
	}

	public List<CartItrems> getCartItremsByCId(int id) {
		// TODO Auto-generated method stub
		return cartRepo.findByCId(id);
	}

	public void updateCartItrems(CartItrems cart) {
		// TODO Auto-generated method stub
		cartRepo.save(cart);
		
	}

	public void deletecartItermsById(int id) {
		// TODO Auto-generated method stub
		cartRepo.deleteById(id);
		
	}

	

}
