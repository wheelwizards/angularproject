import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { GenderPipe } from './gender.pipe';
import { HttpClientModule } from '@angular/common/http';
import { ProductComponent } from './product/product.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordsComponent } from './forgot-passwords/forgot-passwords.component';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { BrandsComponent } from './brands/brands.component';
import { CardsComponent } from './cards/cards.component';
import { CategoryComponent } from './category/category.component';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';
import { OdersComponent } from './oders/oders.component';
import { AdminComponent } from './admin/admin.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProductsComponent } from './edit-products/edit-products.component';
import { ModifinigProductComponent } from './modifinig-product/modifinig-product.component';
import { AddProductComponent } from './add-product/add-product.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    RegisterComponent,
    GenderPipe,
    ProductComponent,
    LogoutComponent,
    ForgotPasswordsComponent,
    HomeComponent,
    AboutUsComponent,
    ContactComponent,
    CarouselComponent,
    FooterComponent,
    BrandsComponent,
    CardsComponent,
    CategoryComponent,
    AddToCartComponent,
    OdersComponent,
    AdminComponent,
    EditPageComponent,
    UserProfileComponent,
    EditProductsComponent,
    ModifinigProductComponent,
    AddProductComponent
  
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,  
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
