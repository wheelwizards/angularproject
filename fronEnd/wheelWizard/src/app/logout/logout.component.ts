import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrl: './logout.component.css'
})
export class LogoutComponent {

  //Dependency Injection for EmpService and Router
  constructor(private service: UserService, private router: Router,private toastr: ToastrService) {
    localStorage.removeItem('user');
    this.toastr.success('Successfully Logged Out!!!','success');
    this.service.setUserLoggedOut();
    this.service.setAdminLoggedOut();
    this.router.navigate(['']);
  }

}
