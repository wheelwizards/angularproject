import { JsonPipe } from '@angular/common';
import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-to-cart',
  templateUrl: './add-to-cart.component.html',
  styleUrl: './add-to-cart.component.css'
})
export class AddToCartComponent  implements OnInit {
  cartProducts: any;
  products: any;
 cartItem:any;
  cartCount:number;
  total: number;
  user:any;
  temp:any;
  i:number;
  
  loginStatus:any;
  result:any;
  constructor(private router: Router,private service: UserService,private toastr: ToastrService) {
   this.i=0;
    this.cartCount = 0;
    this.temp=localStorage.getItem('user');
    this.user=JSON.parse(this.temp);
    console.log(this.user);
    this.cartItem=[]
    this.total = 0;
    this.products  = localStorage.getItem('cartProducts');
    this.cartProducts = JSON.parse(this.products);
  }
  async ngOnInit(){
  await  this.service.getUserLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
    console.log(this.loginStatus)
    // if(this.loginStatus){
    await this.service.getYourCart(this.user.id).then((data: any) => {
      this.cartItem=data;
      });
      let size=0;
      if(size<this.cartItem.length ||size>this.cartItem.length){
        await this.service.getYourCart(this.user.id).then((data: any) => {
          this.cartItem=data;
          });
         
      }
      size=this.cartItem.length;
      console.log(this.cartItem);
      if (this.cartItem != null) {
        this.cartItem.forEach((element: any) => {
          this.total += Math.ceil( element.cost);
         
        });
      }
     
      
      console.log(this.cartItem)
    // }
      // else{
      //   this.cartItem=[];
      // }
  }
 
  async addQuantity(index:any){
    // this.cartItem[index].quantity += 1;
    // this.cartItem[index].cost +=Math.round(this.cartItem[index].cost)/ (this.cartItem[index].quantity)
    await this.service.setTempCart(this.cartItem[index]);
    await this.service.getYourCart(this.user.id).then((data: any) => {
      this.cartItem=data;
      });

    this.total=0;
    this.cartItem.forEach((element: any) => {
      this.total += Math.ceil( element.cost);
     
    });
   
  }
  async decrement(index:any ,sNo:any){
    console.log(index)
    await this.service.decrement(this.cartItem[index]);
    await this.service.getYourCart(this.user.id).then((data: any) => {
      this.cartItem=data;
      });
      console.log(this.cartItem[index].quantity);
      if(this.cartItem[index].quantity==0){
        console.log("delete"+index);
       this.delectItem(index,sNo);
      }
      await this.service.getYourCart(this.user.id).then((data: any) => {
        this.cartItem=data;
        });

    this.total=0;
    this.cartItem.forEach((element: any) => {
      this.total += Math.ceil( element.cost);
     
    });
   
  }
  async delectItem(index:any,sNo:any){
    console.log("delete"+index);
  await  this.service.deleteTempCart(sNo);
    this.cartItem.splice(index, 1);
    this.total=0;
      this.cartItem.forEach((element: any) => {
        this.total += Math.ceil( element.cost);
       
      });
    
   
  }
 async purchase() {
    console.log(this.cartItem);
    this.result= await this.service.setCartIterms(this.cartItem);
    console.log(this.result);
    // alert('Thank You for Purchasing');
    this.toastr.success('Thank You for Purchasing...!', 'success');
    this.cartProducts = [];
    localStorage.removeItem('cartProducts');
    this.service.resetCount();
    this.router.navigate(['']);
  }
  
}
