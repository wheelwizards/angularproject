import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormControl,Validator,FormBuilder,FormGroup } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent implements OnInit  {
  user: any;
  countries: any;
 

  constructor(private service: UserService, private router: Router,private toastr: ToastrService) {
     
    this.user = {
      userName: '',
      address:'',
      gender: '',
      dob: '',
      mobileNo: '',
      emailId: '',
      password: '',
      // confromPassword:'',
    }
  }

  ngOnInit() {
    this.service.getAllContries().subscribe((data: any) => { this.countries = data; });
  }


  registerSubmit (registerForm: any) {
    // if(registerForm.password.toEqual(registerForm.confromPassword)){
    //   this.flag=false;
    // }else{
  this.user=registerForm
    console.log(this.user);
    this.service.registerUser(this.user).subscribe((data: any) => {
      console.log(data);
      this.toastr.success('registration ','success');
      this.router.navigate(['login']);
    });
  // }
  // return true;
  }


}
