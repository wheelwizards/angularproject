import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { OdersComponent } from './oders/oders.component';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordsComponent } from './forgot-passwords/forgot-passwords.component';
import { authGuard } from './auth.guard';
import { HomeComponent } from './home/home.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactComponent } from './contact/contact.component';
import { ProductComponent } from './product/product.component';
import { AdminComponent } from './admin/admin.component';
import { AddToCartComponent } from './add-to-cart/add-to-cart.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { EditProductsComponent } from './edit-products/edit-products.component';
import { ModifinigProductComponent } from './modifinig-product/modifinig-product.component';
import { AddProductComponent } from './add-product/add-product.component';
const routes: Routes = [
  {path:'',  canActivate:[authGuard],component:HomeComponent},
  {path:'contact',     component:ContactComponent},
  {path:'login',        canActivate:[authGuard],component:LoginComponent},
  {path:'register',   canActivate:[authGuard], component:RegisterComponent},
  {path:'forgotPassword',     canActivate:[authGuard], component:ForgotPasswordsComponent},
  {path:'logout',    canActivate:[authGuard], component:LogoutComponent},
  {path:'aboutUs',  canActivate:[authGuard],component:AboutUsComponent},
  {path:'product',  canActivate:[authGuard],component:ProductComponent},
  {path:'cart',      canActivate:[authGuard],component:AddToCartComponent},
  {path:'oders',     component:OdersComponent},
  {path:'admin',     component:AdminComponent},
  {path:'editPage', component:EditPageComponent},
  {path:'profile', component:UserProfileComponent},
  {path:'editProducts', component:EditProductsComponent},
  {path:'modifyProduct', component:ModifinigProductComponent},
  {path:'addProduct', component:AddProductComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
