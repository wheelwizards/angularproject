import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-edit-products',
  templateUrl: './edit-products.component.html',
  styleUrl: './edit-products.component.css'
})
export class EditProductsComponent implements OnInit {
  allProducts:any;
  product:any;

  constructor(private sevice :UserService,
    private router :Router
  ){
   

  }
 async ngOnInit() {
   await this.sevice.getAllProduct().then((data: any) => {
      this.allProducts=data;
      });

  }
 async deletProduct(id:any){
    await this.sevice.deleteProductById(id);
    await  this.sevice.getAllProduct().then(users=>{
      this.allProducts=users;
  });
  }
  editProduct(id :any){
   this.router.navigate(['modifyProduct'], { queryParams: { id:id  } });
  }
  addProduct(){
    this.router.navigate(['addProduct']);
  }
}
