import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrl: './admin.component.css'
})
export class AdminComponent implements OnInit {
  allusers:any;
constructor(private service: UserService,
private router :Router
){
 
}
async ngOnInit(){
    await  this.service.getAllUserInfo().then(users=>{
          this.allusers=users;
    });
    console.log(this.allusers)
}
async deletUserInfo(id:any){
  await this.service.deleteUserInfo(id);
  await  this.service.getAllUserInfo().then(users=>{
    this.allusers=users;
});
}
UsereditPage(id:any){
  this.router.navigate(['editPage'], { queryParams: { id:id  } })
}

}
