import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

private adminStatus = new BehaviorSubject<boolean>(false);
private loginStatus = new BehaviorSubject<boolean>(false);
private cartCountSubject = new Subject<number>();
  cartCount$ = this.cartCountSubject.asObservable();


constructor(private http: HttpClient) {
 
  

}
udateUserInfoByEmail(email:String,password:String){
  return this.http.get('http://localhost:8085/udateUserInfoByEmail/'+email+"/"+password).toPromise();

}
setUserLoggedIn() {
  this.loginStatus.next(true);
}
setUserLoggedOut() {
  this.loginStatus.next(false);
}
setAdminLoggedIn() {
  this.adminStatus.next(true);
}
setAdminLoggedOut() {
  this.adminStatus.next(false);
}
setTempCart(carts :any){
  return this.http.put('http://localhost:8085/addIterms',carts).toPromise();
}
deleteTempCart (index:any) :any{
  console.log(index);
  return this.http.delete('http://localhost:8085/deleteById/'+index).toPromise();
}
getProduct( key:Number){
  return this.http.get('http://localhost:8085/getBycatogiryId/'+key).toPromise();
}
getAllProduct(){
  return this.http.get('http://localhost:8085/getAllProducts').toPromise();
}
deleteProductById (index:any) :any{
  console.log(index);
  return this.http.delete('http://localhost:8085/deleteProductById/'+index).toPromise();
}
getProdutById(id:any){
  return this.http.get('http://localhost:8085/getById/'+id).toPromise();
}
updateProduct(userData:any){
  return this.http.put('http://localhost:8085/updateProduct', userData).toPromise();
}
getUserLoginStatus(): any {
  return this.loginStatus.asObservable();
}
getAdminLoginStatus(): any {
  return this.adminStatus.asObservable();
}
setCartIterms(cart:any){
  console.log(cart)
  return this.http.post('http://localhost:8085/cartIterms',cart).toPromise();
}
getAllContries() {
  return this.http.get('https://restcountries.com/v3.1/all');
}
userLogin(emailId: any, password: any) {
  return this.http.get('http://localhost:8085/userLogin/' + emailId + "/" + password).toPromise();
}
getYourOders(userId:any){
  return this.http.get('http://localhost:8085/getOdders/' + userId).toPromise();
}
getYourCart(userId:any){
  return this.http.get('http://localhost:8085/getCart/' + userId).toPromise();
}
getLoginStatus(): Observable<boolean>  {
  return this.loginStatus.asObservable();
}
getAdminStatus(): Observable<boolean> {
   return this.adminStatus.asObservable();
}
registerUser(employee: any) {
  console.log(employee)
  return this.http.post('http://localhost:8085/addUserInfo', employee);
}
getUserInfoByEmail(email:any){
  return this.http.get('http://localhost:8085/getUserInfoByEmail', email).toPromise();
}
updateUserInfo(userData:any){
  return this.http.put('http://localhost:8085/updateUserInfo', userData).toPromise();
}
decrement(carts :any){
  console.log(carts);
  return this.http.put('http://localhost:8085/decrementTempCart',carts).toPromise();
}

updateCartCount(count: number) {
  this.cartCountSubject.next(count);
  }
 resetCount() {
  this.cartCountSubject.next(0);
}
getUserInfoById(id:any){
  return this.http.get('http://localhost:8085/getUserInfoById/'+id).toPromise();
}
getAllUserInfo(){
  return this.http.get('http://localhost:8085/getAllUserInfo').toPromise();
}
deleteUserInfo(id:any){
  return this.http.delete('http://localhost:8085/deleteUserInfoById/'+id).toPromise();
}
}

