import { Component,OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-forgot-passwords',
  templateUrl: './forgot-passwords.component.html',
  styleUrl: './forgot-passwords.component.css'
})
export class ForgotPasswordsComponent {
  user:any;
  
constructor(private service :UserService,private router:Router, private toastr: ToastrService){
  // this.user = {
  //   userName: '',
  //   address:'',
  //   gender: '',
  //   dob: '',
  //   mobileNo: '',
  //   emailId: '',
  //   password: ''
   
  // }

}

 async forgotPasswordSubmit(newData:any){
  console.log(newData.emailId+" "+newData.password)
    this.user= await this.service.udateUserInfoByEmail(newData.emailId,newData.password);
 console.log(this.user);
 if(this.user!=null){
       this.toastr.success('updated sucessfuly', 'Toastr fun!');
       this.router.navigate(['login']);
 }
}
}
