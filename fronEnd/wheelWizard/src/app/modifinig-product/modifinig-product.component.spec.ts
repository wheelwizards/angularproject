import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifinigProductComponent } from './modifinig-product.component';

describe('ModifinigProductComponent', () => {
  let component: ModifinigProductComponent;
  let fixture: ComponentFixture<ModifinigProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ModifinigProductComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ModifinigProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
