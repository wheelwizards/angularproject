import { JsonPipe } from '@angular/common';
import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-oders',
  templateUrl: './oders.component.html',
  styleUrl: './oders.component.css'
})
export class OdersComponent implements OnInit {
  logInStatus :any;
  temp:any;
  user:any;
  oders:any;
  constructor(private service :UserService,private router:Router){
    this.temp=localStorage.getItem('user');
    this.user=JSON.parse(this.temp);
    this.service.getYourOders
    this.logInStatus=this.service.getLoginStatus();
    if(this.logInStatus){
     
   
    }
  }
 async ngOnInit() {
    await this.service.getYourOders(this.user.id).then((data: any) => {
      this.oders=data;
      });
      console.log(this.oders);
  }
   

}
