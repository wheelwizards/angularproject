package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CartItermsDao;
import com.model.CartItrems;
import com.model.Product;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class CartItermsController {
	
	@Autowired
	CartItermsDao cartDao;
	@PostMapping("cartIterms")
	public void setCartItrems(@RequestBody List<CartItrems> cartIterms) {
		cartDao.setCartItrems(cartIterms);
	}
	@GetMapping("getOdders/{id}")
	public List<CartItrems> getCartItremsByCId(@PathVariable("id") int id ){
		return cartDao.getCartItremsByCId(id);	
	}
	@PostMapping("updatecartIterms")
	public void updateCartItrems(@RequestBody CartItrems cart) {
		
		 cartDao.updateCartItrems(cart);
	}
	
	@DeleteMapping("deletecartItermsById/{id}")
	public void deletecartItermsById(@PathVariable("id") int id) {
		cartDao.deletecartItermsById(id);
		
	}

}
