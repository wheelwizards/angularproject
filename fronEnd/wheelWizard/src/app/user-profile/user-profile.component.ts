import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrl: './user-profile.component.css'
})
export class UserProfileComponent {
temp:any;
user:any;
constructor(){
  this.temp=localStorage.getItem('user');
  this.user=JSON.parse(this.temp);
}
}