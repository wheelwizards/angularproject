import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(userName: any, gender: any): any {

    if (gender == 'Male')
      return 'MR.' + userName;
    else if (gender == 'Female')
      return 'Miss.' + userName;
    return userName;
  }


}
