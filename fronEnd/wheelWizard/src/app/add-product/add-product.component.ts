import { Component ,OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrl: './add-product.component.css'
})
export class AddProductComponent  {
  product:any;
  id:any;
  constructor(private service: UserService,private route: ActivatedRoute,private router: Router){
      
  }
 async addProduct(item:any){
  this.product= {
    name: item.name,
     imgsrc: item.imgsrc,
     categoryID: item.categoryID,
   cost: item.cost,
    desciption: item.description
 }
   console.log(this.product);
 await  this.service.updateProduct(this.product);
 this.router.navigate(['editProducts']);
 } 
  }
 