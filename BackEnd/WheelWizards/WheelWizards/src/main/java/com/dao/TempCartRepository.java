
package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.model.TempCart;

@Repository
public interface TempCartRepository extends JpaRepository<TempCart, Integer> {

	@Query("from TempCart e where e.user.id = :id")
	List<TempCart> findByCId(@Param("id") int id);
	@Query("from TempCart e where e.cartId= :cartId")
	TempCart findByCartId(@Param("cartId") int cartId);
	
//	@Query("delete from TempCart e where e.cartId=cartId;")
//	void deleteByCartId(@Param("cartId") int cartId);

}
